from prettytable import PrettyTable
from sentiment_analyze import preprocessing
from collections import Counter
import matplotlib.pyplot as plt
from pymongo import MongoClient
from sklearn.feature_extraction.text import CountVectorizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical
client = MongoClient('mongodb://localhost:27017/test_twitter_db')
dbs = client.test_twitter_db
iran_sent = dbs.iran_sent
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import sys

class analyse:
	def __init__(self):

		self.senti = []
		self.word = []

	def pie_graph(self):

		tweet = iran_sent.find({})
		for t in tweet:
			self.senti.append(t["sentiment"])
			self. word.append(t["word"])
		mount = Counter(self.senti)
		print(mount)
		import matplotlib.pyplot as plt
		slices_hours = mount.values()
		activities = mount.keys()
		colors = ['g', 'r' ,'b']

		plt.pie(slices_hours, labels=activities, colors=colors, startangle=90, autopct='%.1f%%')
		plt.show(s)


	def dent_predict(self):
		tweet = iran_sent.find({},{"word":1})
		text =[]
		sent=[]
		max_fatures = 2000
		for t in tweet:
			text .append(" ".join(t["word"]))
		tokenizer = Tokenizer(num_words=max_fatures, split=' ')
		tokenizer.fit_on_texts(text)

		t = tokenizer.texts_to_sequences(text)
		X = pad_sequences(t)
		print(X)
		embed_dim = 128
		lstm_out = 196

		model = Sequential()
		model.add(Embedding(max_fatures, embed_dim, input_length=X.shape[1]))
		model.add(SpatialDropout1D(0.4))
		model.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
		model.add(Dense(3, activation='softmax'))
		model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
		print(model.summary())
		senti = iran_sent.find({},{"sentiment":1})
		for t in senti:
			sent.append(" ".join(t["sentiment"]))
		Y = pd.get_dummies(sent)
		#print(Y)
		X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=42)
		print(X_train.shape, Y_train.shape)
		print(X_test.shape, Y_test.shape)
		batch_size = 32
		model.fit(X_train, Y_train, epochs=7, batch_size=batch_size, verbose=2)
		validation_size = 1500

		X_validate = X_test[-validation_size:]
		Y_validate = Y_test[-validation_size:]
		X_test = X_test[:-validation_size]
		Y_test = Y_test[:-validation_size]
		score, acc = model.evaluate(X_test, Y_test, verbose=2, batch_size=batch_size)
		print("score: %.2f" % (score))
		print("acc: %.2f" % (acc))


if __name__=='__main__':
	a = analyse()
	a.dent_predict()