from validation import Validation
from postagging import Tag
from negation import NegatingWordReader
from modifier import ModifierWordReader
from preprocessing import PreProcessing
from GetSentimentFromLevel import Sentiment
import sys
while(True):
	tweet = input("whats happening ? ")
	#tweet = "im not sad "
	p = PreProcessing(tweet)
	tag = Tag(p.process())
	# tag = Tag(['verify', 'the', 'happy', 'love', 'is ', 'honey'])
	val = Validation(tag.pos_tagging())
	validate = val.filter_tweet()
	senti = Sentiment(validate)
	if senti.result["sentiment"] =="Positive":
		print("great you are happy :}")
	elif senti.result["sentiment"] =="Negative":
		print("why are you sad dear :(")
	else: print("logical !")