
emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP\>\}\{\<\|] # Mouth
    )"""

regex_str = [
    emoticons_str,
    r'<[^>]+>',  # HTML tags
    r'(?:@[\w_]+)',  # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)",  # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+',  # URLs

    r'(?:(?:\d+,?)+(?:\.?\d+)?)',  # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])",  # words with - and '
    r'(?:[\w_]+)',  # other words
    # r'(?:\S)'   # anything else
]
emoticons_happy = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\)\>\{\]] # Mouth
    )"""

emoticons_sad = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [\[\(\[\<\[\{\|\\\/] # Mouth
    )"""
emoticons_joke = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [pP] # Mouth
    )"""

emotion = [
    emoticons_happy,  # happy emotion
    emoticons_sad,  # SAD emotion
    emoticons_joke,        # joke
           ]

neg_reviews = []
pos_reviews = []
useful_word =[]
useful_word_dict = []